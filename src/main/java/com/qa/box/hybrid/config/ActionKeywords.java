package com.qa.box.hybrid.config;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.qa.box.hybrid.executionEngine.DriverScript.OR;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.qa.box.hybrid.executionEngine.DriverScript;
import com.qa.box.hybrid.utility.Log;
//import com.qa.hs.keyword.engine.CustomFailureExeception;

public class ActionKeywords {
	
	
		public static WebDriver driver;
		public static String webElement;
			
	public static void openBrowser(String object,String data){		
		Log.info("Opening Browser");
		try{				
			if(data.equals("Mozilla")){
				driver=new FirefoxDriver();
				Log.info("Mozilla browser started");				
				}
			else if(data.equals("IE")){
				//Dummy Code, Implement you own code
				driver=new InternetExplorerDriver();
				Log.info("IE browser started");
				}
			else if(data.equals("Chrome")){
				//Dummy Code, Implement you own code  C:\mufg_demo\Driver\chromedriver.exe
				System.setProperty("webdriver.chrome.driver", "C:/mufg_demo/Driver/chromedriver.exe");
				driver=new ChromeDriver();
				Log.info("Chrome browser started");
				driver.manage().window().maximize();
				driver.manage().deleteAllCookies();
					
				driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
			
			int implicitWaitTime=(10);
			driver.manage().timeouts().implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
		}catch (Exception e){
			Log.info("Not able to open the Browser --- " + e.getMessage());
			DriverScript.bResult = false;
		}
	}
	
	public static void navigate(String object, String data){
		try{
			Log.info("Navigating to URL");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.get(Constants.URL);
		}catch(Exception e){
			Log.info("Not able to navigate --- " + e.getMessage());
			DriverScript.bResult = false;
			}
		}
	
	public static void simpleClick(String object, String data){
		try{
			Log.info("Clicking on Webelement "+ object);
			driver.findElement(By.xpath(OR.getProperty(object))).click();

		 }catch(Exception e){	
			  
 			Log.error("Not able to click --- " + e.getMessage());
 			DriverScript.bResult = false;
         	}
		}
	
	public static void switchToFrame(String object, String data){
		try{
			Log.info("Switching On Frame "+ object);
			
			driver.switchTo().frame(object);
			
		 }catch(Exception e){	
			  
 			Log.error("Frame Element is not found  --- " + e.getMessage());
 			DriverScript.bResult = false;
         	}
		}
	
	
	public static void click(String object, String data ){
		try{
			Log.info("Clicking on Webelement "+ object);
			
			getWebElement(object).click();			
		 }catch(Exception e){	
			  
 			try {
 				clickFromJScript(object,data);
				
			} catch (Exception e2) {
				e2.printStackTrace();
				Log.error("Not able to click --- " + e2.getMessage());
				DriverScript.bResult = false;
			}
         	}
		}
	

	
	public static void clickFromJScript(String object, String data){
		try{
			Log.info("Clicking on Webelement "+ object);
			WebElement ele = driver.findElement(By.xpath(object));
			
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", ele);

		 }catch(Exception e){	
			  
 			Log.error("Not able to click --- " + e.getMessage());
 			DriverScript.bResult = false;
         	}
		}
	
	public static void isDisplayed(String object, String data){
		try{
			Log.info("Verifying Webelement "+ object);
			driver.findElement(By.xpath(OR.getProperty(object))).isDisplayed();
		 }catch(Exception e){
 			Log.error("Webelement Not Presnet --- " + e.getMessage());
 			DriverScript.bResult = false;
         	}
		}
	
	
	public static void input(String object, String data){
		try{
			Log.info("Entering the text in " + object);
			driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(data);
		 }catch(Exception e){
			 Log.error("Not able to Enter UserName --- " + e.getMessage());
			 DriverScript.bResult = false;
		 	}
		}
	
	
	public void moveOverElement(String object,String data)
    {
		
		try {
			Log.info("Moving On Element " + object);
			WebElement ele = driver.findElement(By.xpath(OR.getProperty(object)));
			Actions actions = new Actions(driver);
			actions.clickAndHold(ele).moveByOffset(0, 500).release().perform();
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			Log.error("Not able to move on Element --- " + e.getMessage());
			 DriverScript.bResult = false;
		}

    }
	

	public static void waitFor(String object, String data) throws Exception{
		try{
			Log.info("Wait for 5 seconds");
			Thread.sleep(5000);
		 }catch(Exception e){
			 Log.error("Not able to Wait --- " + e.getMessage());
			 DriverScript.bResult = false;
         	}
		}
	
	

	public static void closeBrowser(String object, String data){
		try{
			Log.info("Closing the browser");
			driver.quit();
		 }catch(Exception e){
			 Log.error("Not able to Close the Browser --- " + e.getMessage());
			 DriverScript.bResult = false;
         	}
		}
	
	
	public static void VerifyText(String object, String data )  {
		
		try {
			Log.info("Verifying the Text " + object);
			String WebText = driver.findElement(By.xpath(OR.getProperty(object))).getText();
			if(WebText.equalsIgnoreCase(data)) {
				
				Log.info("Verification of the Text success " +data);
			}
			
		} catch (Exception e) {
			Log.error("Verification of the Text not find --- " + e.getMessage());
			 DriverScript.bResult = false;
		}
	
		
	}
	
	 /**
	    * This Method will return web element.
	    * @param locator
	    * @return
	    * @throws Exception
	    */
		public static WebElement getLocator(String locator) throws Exception {
			
			  String[] split = locator.split(":"); 
			  String locatorType = split[0]; 
			  String locatorValue = split[1];
			 
			//String locatorType =null; 
			//String locatorValue = null;
			if (locatorType.toLowerCase().equals("id"))
				return driver.findElement(By.id(locatorValue));
			else if (locatorType.toLowerCase().equals("name"))
				return driver.findElement(By.name(locatorValue));
			else if ((locatorType.toLowerCase().equals("classname"))
					|| (locatorType.toLowerCase().equals("class")))
				return driver.findElement(By.className(locatorValue));
			else if ((locatorType.toLowerCase().equals("tagname"))
					|| (locatorType.toLowerCase().equals("tag")))
				return driver.findElement(By.className(locatorValue));
			else if ((locatorType.toLowerCase().equals("linktext"))
					|| (locatorType.toLowerCase().equals("link")))
				return driver.findElement(By.linkText(locatorValue));
			else if (locatorType.toLowerCase().equals("partiallinktext"))
				return driver.findElement(By.partialLinkText(locatorValue));
			else if ((locatorType.toLowerCase().equals("cssselector"))
					|| (locatorType.toLowerCase().equals("css")))
				return driver.findElement(By.cssSelector(locatorValue));
			else if (locatorType.toLowerCase().equals("xpath"))
				return driver.findElement(By.xpath(locatorValue));
			else
				throw new Exception("Unknown locator type '" + locatorType + "'");
		}
		
		
		public static List<WebElement> getLocators(String locator) throws Exception {
	        //String[] split = locator.split(":");
			//String locatorType = split[0];
			//String locatorValue = split[1];
			
			String locatorType =null; 
			String locatorValue = null;

			if (locatorType.toLowerCase().equals("id"))
				return driver.findElements(By.id(locatorValue));
			else if (locatorType.toLowerCase().equals("name"))
				return driver.findElements(By.name(locatorValue));
			else if ((locatorType.toLowerCase().equals("classname"))
					|| (locatorType.toLowerCase().equals("class")))
				return driver.findElements(By.className(locatorValue));
			else if ((locatorType.toLowerCase().equals("tagname"))
					|| (locatorType.toLowerCase().equals("tag")))
				return driver.findElements(By.className(locatorValue));
			else if ((locatorType.toLowerCase().equals("linktext"))
					|| (locatorType.toLowerCase().equals("link")))
				return driver.findElements(By.linkText(locatorValue));
			else if (locatorType.toLowerCase().equals("partiallinktext"))
				return driver.findElements(By.partialLinkText(locatorValue));
			else if ((locatorType.toLowerCase().equals("cssselector"))
					|| (locatorType.toLowerCase().equals("css")))
				return driver.findElements(By.cssSelector(locatorValue));
			else if (locatorType.toLowerCase().equals("xpath"))
				return driver.findElements(By.xpath(locatorValue));
			else
				throw new Exception("Unknown locator type '" + locatorType + "'");
		}
		
		
		
	
		public static WebElement getWebElement(String object) throws Exception{
			System.out.println("locator data:-"+object+"is---"+OR.getProperty(object));
			return getLocator(OR.getProperty(object));
		}
		
		public static List<WebElement> getWebElements(String locator) throws Exception{
			return getLocators(OR.getProperty(locator));
		}
	
	

	}