package com.qa.box.hybrid.executionEngine;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PdfBoxVerification {

	@Test
	public void readPDFTest() throws IOException {
		
		URL url = new URL("http://www.africau.edu/images/default/sample.pdf");
		
		InputStream ls = url.openStream();
		
		BufferedInputStream fileparse = new BufferedInputStream(ls);
		
		
		PDDocument document  = null;
		document = PDDocument.load(fileparse);
		
		String pdfContent = new PDFTextStripper().getText(document);
		
		System.out.println(pdfContent);
		
		
		Assert.assertTrue(pdfContent.contains("A Simple PDF File"));
		Assert.assertTrue(pdfContent.contains("Simple PDF File 2"));
		Assert.assertTrue(pdfContent.contains("...continued from page 1. Yet more text. And more text. And more text."));
	}
	

}
